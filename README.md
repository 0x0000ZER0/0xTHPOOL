# 0xTHP00L

A thread pool implementation using circular queue.

### API

- Creating the pool.

```c
z0_thpool thpool;
z0_thpool_init(&thpool, NUMBER_OF_TASKS, NUMBER_OF_THREADS);
//...
z0_thpool_free(&thpool);
```

- Adding a task to the queue.

```c
static void*
func(void *data) {
        printf("=============== %i =================\n", *(int*)data);

        return NULL;
}

//...
int val;
val = 3;

z0_task task;
task.func = func;
task.arg  = (void*)&val;

z0_thpool_push(&thpool, &task);
```

- Waiting for the threads.

```c
z0_thpool_wait(&thpool);
```

- Getting the number of cores.

```c
long cores;
cores = z0_thpool_get_cores_count();
```
