
main: main.c z0_thpool.c
	gcc main.c z0_thpool.c -O2 -o 0xTHPOOL -pedantic -Wall -lpthread

debug:
	gcc main.c z0_thpool.c -O2 -o 0xTHPOOL -pedantic -Wall -lpthread -DZ0_DEBUG

clear:
	rm 0xTHPOOL
	
