#include "z0_thpool.h"

#include <stdlib.h>

#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif

#ifdef Z0_DEBUG
#include <stdio.h>
#endif

static void*
z0_thpool_handle(void *data) {
	z0_thpool *thpool;
	thpool = (z0_thpool*)data;

	pthread_mutex_lock(&thpool->mtx);
	while (thpool->len == 0)
		pthread_cond_wait(&thpool->cond, &thpool->mtx);

	thpool->head = (thpool->head + 1) % thpool->cap;
	
	--thpool->len;
 	pthread_mutex_unlock(&thpool->mtx);
	
#ifdef Z0_DEBUG
	printf("Z0 THPOOL DEBUG: started.\n");
#endif

	thpool->queue[thpool->head].func(thpool->queue[thpool->head].arg);

#ifdef Z0_DEBUG
	printf("Z0 THPOOL DEBUG: completed.\n");
#endif

	return NULL;
}

void
z0_thpool_init(z0_thpool *thpool, uint32_t cap, uint32_t max) {
	thpool->cap   = cap;
	thpool->head  = 0;
	thpool->tail  = 0;
	thpool->len   = 0;
	thpool->queue = malloc(sizeof (z0_task) * cap);
	thpool->max   = max;
	thpool->thds  = malloc(sizeof (pthread_t) * max);
	
	pthread_mutex_init(&thpool->mtx, NULL);
	pthread_cond_init(&thpool->cond, NULL);	

	int32_t i;
	i = 0;

	while (i < max) {
#ifdef Z0_DEBUG
		printf("Z0 THPOOL DEBUG: thread created.\n");
#endif
		pthread_create(&thpool->thds[i], NULL, z0_thpool_handle, (void*)thpool);
		++i;	
	}
}

bool
z0_thpool_push(z0_thpool *thpool, z0_task *task) {
	pthread_mutex_lock(&thpool->mtx);
	if (thpool->len + 1 == thpool->cap) {
		pthread_mutex_unlock(&thpool->mtx);
		return false;
	}

	thpool->tail = (thpool->tail + 1) % thpool->cap;	
	++thpool->len;

#ifdef Z0_DEBUG
	printf("Z0 THPOOL DEBUG: HEAD = %i\n", thpool->head);
	printf("Z0 THPOOL DEBUG: TAIL = %i\n", thpool->tail);
	printf("Z0 THPOOL DEBUG: LEN  = %i\n", thpool->len);
#endif
	thpool->queue[thpool->tail].func = task->func; 	
	thpool->queue[thpool->tail].arg  = task->arg;

	pthread_cond_signal(&thpool->cond); 	
	pthread_mutex_unlock(&thpool->mtx);

	return true;
}

void
z0_thpool_wait(z0_thpool *thpool) {
	int32_t i;
	i = 0;

	while (i < thpool->max) {
		pthread_join(thpool->thds[i], NULL);
		++i;
	}
}

long
z0_thpool_get_cores_count() {
#ifdef _WIN32
    	SYSTEM_INFO info;
    	GetSystemInfo(&info);

    	return info.dwNumberOfProcessors;
#else
	return sysconf(_SC_NPROCESSORS_ONLN);
#endif
}

void
z0_thpool_free(z0_thpool *thpool) {
	pthread_cond_destroy(&thpool->cond);
	pthread_mutex_destroy(&thpool->mtx);

	free(thpool->thds);
	free(thpool->queue);
}
