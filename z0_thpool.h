#ifndef Z0_THPOOL_H
#define Z0_THPOOL_H

#include <stdint.h>
#include <pthread.h>
#include <stdbool.h>

typedef void* (*z0_thpool_func)(void*);

typedef struct {
	z0_thpool_func  func;
	void 		 *arg;
} z0_task;

typedef struct {
	uint32_t   	   cap;        // 4 bytes
	volatile uint32_t  head;       // 4 bytes
	volatile uint32_t  tail;       // 4 bytes
	volatile uint32_t  len;        // 4 bytes
	z0_task           *queue;      // 8 bytes
	uint32_t 	   max;        // 4 bytes
	pthread_t 	  *thds;       // 8 bytes
	pthread_mutex_t    mtx;        // 8 bytes
	pthread_cond_t     cond;       // 8 bytes

	uint8_t		 padding[12]; // extra padding to fit the data in the L1.
} z0_thpool;

void
z0_thpool_init(z0_thpool*, uint32_t, uint32_t);

bool
z0_thpool_push(z0_thpool*, z0_task*);

void
z0_thpool_wait(z0_thpool*);

long
z0_thpool_get_cores_count();

void
z0_thpool_free(z0_thpool*);

#endif
